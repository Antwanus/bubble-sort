package com.av;

import java.security.SecureRandom;
import java.util.Random;
import java.util.logging.Logger;

public class Main {
   public static final Logger LOG = Logger.getLogger(Main.class.getName());
   private static final Random RANDOM = new SecureRandom();

   public static void main(String[] args) {
      int[] numbers = new int[10000];

      for (int i = 0; i < numbers.length; i++)
         numbers[i] = RANDOM.nextInt(100);

      LOG.info("Unsorted array:");
      printArray(numbers);

      bubbleSort(numbers);

      LOG.info("Sorted array:");
      printArray(numbers);
   }

   private static void bubbleSort(int[] numbers) {
      boolean isSwapped = false;
      int j = 1;

      for (int i = 0; i < numbers.length - 1; i++) {
         if(numbers[i] > numbers[j]) {
            isSwapped = true;
            var tmp = numbers[i];
            numbers[i] = numbers[j];
            numbers[j] =  tmp;
         }
         j++;
      }
      if(isSwapped) // the array is likely not fully ordered
         bubbleSort(numbers);
   }


   private static void printArray(int[] numbers) {
      StringBuilder result = new StringBuilder("[");
      int lastIndex = numbers.length - 1;

      for (int i = 0; i < numbers.length; i++) {
         result.append(numbers[i]);
         if (i != lastIndex)
            result.append(", ");

      }
      result.append("]");

      LOG.info(String.valueOf(result));
   }
}

